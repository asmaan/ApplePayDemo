//
//  main.m
//  ApplePayDemo
//
//  Created by A S Maan on 2/8/16.
//  Copyright © 2016 ASMaan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

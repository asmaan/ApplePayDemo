//
//  AppDelegate.h
//  ApplePayDemo
//
//  Created by A S Maan on 2/8/16.
//  Copyright © 2016 ASMaan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


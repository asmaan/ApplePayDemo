//
//  ViewController.m
//  ApplePayDemo
//
//  Created by A S Maan on 2/8/16.
//  Copyright © 2016 ASMaan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<PKPaymentAuthorizationViewControllerDelegate>
@property (strong, nonatomic) NSArray *supportedPayments;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _supportedPayments = [NSArray arrayWithObjects:PKPaymentNetworkVisa,PKPaymentNetworkMasterCard,PKPaymentNetworkAmex,nil];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) btnApplePayPressed : (id) sender {
    PKPaymentRequest *payRequest = [PKPaymentRequest new];//Create new Payment Request
    payRequest.merchantIdentifier = @"merchant.com.timegigo.net";//Give merchant that we generated in Apple DEV account
    payRequest.supportedNetworks = [NSArray arrayWithObjects:PKPaymentNetworkVisa,PKPaymentNetworkMasterCard,PKPaymentNetworkAmex,nil];//Supported networks like Visa,Amex etc.
    payRequest.merchantCapabilities = PKMerchantCapability3DS;//Capabilities of merchant like credit,visa,3D
    payRequest.countryCode = @"US";
    payRequest.currencyCode = @"USD";
    payRequest.requiredShippingAddressFields = PKAddressFieldAll;//Add shipping Address Details
    payRequest.requiredBillingAddressFields = PKAddressFieldAll;//Add Billing Address Details
//    PKShippingMethod *shippingMethod = [PKShippingMethod new];//Add shipping method
//    shippingMethod.amount = [NSDecimalNumber decimalNumberWithString:@"2.5"];//Add shipping amount
//    shippingMethod.identifier = @"Fedex";//Add shipping Description
//    payRequest.shippingMethods = [NSArray arrayWithObject:shippingMethod];
    PKPaymentSummaryItem *item1 = [[PKPaymentSummaryItem alloc] init];//Create for the item price neeeded for payment process
    item1.label = @"Apple iPhone 6";
    item1.amount = [NSDecimalNumber decimalNumberWithString:@"32.5"];
    PKPaymentSummaryItem *itemShipping = [[PKPaymentSummaryItem alloc] init];//Create for shipping price neeeded for the payment process
    itemShipping.label = @"Shipping Charges";
    itemShipping.amount = [NSDecimalNumber decimalNumberWithString:@"2.5"];
    PKPaymentSummaryItem *itemTotal = [[PKPaymentSummaryItem alloc] init];//Create for Total price neeeded for the payment process
    itemTotal.label = @"Total";
    itemTotal.amount = [NSDecimalNumber decimalNumberWithString:@"35.00"];
    payRequest.paymentSummaryItems = @[item1,itemShipping,itemTotal];
    PKPaymentAuthorizationViewController *payVC = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:payRequest];//Create Payment Controller
    payVC.delegate = self;
    [self presentViewController:payVC animated:YES completion:nil];
}
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                  didSelectShippingAddress:(ABRecordRef)address
                                completion:(void (^)(PKPaymentAuthorizationStatus status, NSArray<PKShippingMethod *> *shippingMethods,
                                                     NSArray<PKPaymentSummaryItem *> *summaryItems))completion
{

}
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion
{
    //Write code to push data to your own server
}
- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller
{

}
@end

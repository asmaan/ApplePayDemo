//
//  ViewController.h
//  ApplePayDemo
//
//  Created by A S Maan on 2/8/16.
//  Copyright © 2016 ASMaan. All rights reserved.
//

#import <UIKit/UIKit.h>
@import PassKit;
@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)btnApplePayPressed:(id)sender;
@property (strong, nonatomic) IBOutlet PKPaymentButton *btnApplePay;

@end

